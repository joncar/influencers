<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Administracion extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function bancos($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);            
            $output = $crud->render();
            $this->loadView($output);
        }               
        
        function redes($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);            
            $output = $crud->render();
            $this->loadView($output);
        }        
    }
?>
