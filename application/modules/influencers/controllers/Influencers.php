<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Influencers extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function redes_conectadas($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);            
            $crud->unset_add()->unset_edit()->unset_delete()->unset_print()->unset_export()->unset_read();
            $crud->where('user_id',$this->user->id);
            $crud->display_as('id_red','Identificador')
                 ->display_as('redes_id','RED');
            $crud->columns('redes_id','id_red','nombre','seguidores','fecha_sincronizacion');
            $output = $crud->render();
            $output->title = 'Mis redes';
            $output->output = $this->load->view('crud',array('output'=>$output->output),TRUE);
            $this->loadView($output);
        }               
        
        function bancos_registrados($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);  
            $crud->set_subject('Mis Bancos');
            $crud->set_relation('bancos_id','bancos','bancos_nombre');
            $crud->where('user_id',$this->user->id);
            $crud->display_as('bancos_id','Banco');
            $crud->unset_delete();
            $crud->unset_columns('user_id');
            $crud->field_type('user_id','hidden',$this->user->id);
            $output = $crud->render();
            $output->title = "Mis bancos";
            $this->loadView($output);
        }        
    }
?>
