<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Face extends Panel{        
        function __construct() {
            parent::__construct();
        }
        
        function save(){    
            if(!empty($_POST['data'])){
                $data = json_decode($_POST['data']);
                foreach($data as $d){
                    $yaexiste = $this->db->get_where('redes_conectadas',array('user_id'=>$this->user->id,'redes_id'=>1,'id_red'=>$d->id));
                    $insert = array('posts'=>$d->posts,'likes'=>$d->likes,'seguidores'=>$d->seguidores,'fecha_sincronizacion'=>date("Y-m-d"),'nombre'=>$d->name,'id_red'=>$d->id,'user_id'=>$this->user->id,'redes_id'=>1);
                    if($yaexiste->num_rows()==0){
                        $this->db->insert('redes_conectadas',$insert);
                    }else{
                        $this->db->update('redes_conectadas',$insert,array('user_id'=>$this->user->id,'redes_id'=>1,'id_red'=>$d->id));
                    }
                }
            }                        
        }          
    }
?>
