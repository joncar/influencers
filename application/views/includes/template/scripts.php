<script type="text/javascript" src="<?= base_url() ?>js/template/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.vide.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/superfish.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.easing.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.blockUI.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.qtip.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.actual.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.jplayer.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.scrollTo.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.waypoints.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.countdown.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jplayer.playlist.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.zaccordion.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.supersized.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.nivo.slider.pack.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.touchSwipe.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.infieldlabel.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.carouFredSel.packed.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.fancybox.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.fancybox-media.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.fancybox-buttons.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.windowDimensionListener.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.responsiveElement.js"></script>

<script type="text/javascript" src="<?= base_url() ?>js/template/revslider/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/revslider/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/revslider/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/revslider/extensions/revolution.extension.carousel.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/revslider/extensions/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/revslider/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/revslider/extensions/revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/revslider/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/revslider/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/revslider/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/revslider/extensions/revolution.extension.video.min.js"></script>

<script type="text/javascript" src="<?= base_url() ?>js/template/sticky.min.js"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>  

<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.template.component.js"></script>

<script type="text/javascript" src="<?= base_url() ?>js/template/public.js"></script>