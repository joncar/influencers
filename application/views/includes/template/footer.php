<!-- Footer -->
<div class="template-footer">

    <!-- Top footer -->
    <div class="template-footer-top">

        <!-- Main -->
        <div class="template-main">

            <!-- Widget list -->
            <ul class="template-widget-list template-layout-25x25x25x25 template-clear-fix">

                <!-- Left column -->
                <li class="template-layout-column-left">
                    <h6>
                        <span>Zona Privada</span>
                        <span></span>
                    </h6>
                    <div>
                        <div class="template-widget-text">


                            <p class="template-margin-reset">
                                <a href="post-image.html">
                                    <img src="<?= base_url() ?>img/moodle.jpg" style="width: 75.8%; alt="View Post &quot;Drawing Lesson&quot;"/>
                                </a>
                                <br>
                            </p>
                            <p class="template-margin-reset">
                                <a href="post-image.html">
                                    <img src="<?= base_url() ?>img/edu.jpg" style="width: 75.8%; alt="View Post &quot;Drawing Lesson&quot;"/>
                                </a>
                            </p>


                            <img src="<?= base_url() ?>img/logo_footer.png" class="template-footer-logo template-margin-top-2" alt="" />
                        </div>
                    </div>
                </li>

                <!-- Center left column -->
                <li class="template-layout-column-center-left">
                    <h6>
                        <span>Informacions Mensuals</span>
                        <span></span>
                    </h6>
                    <div>
                        <div class="template-widget-archive template-widget-archive-style-1">
                            <ul>
                                <li>
                                    <a href="blog-page-archive.html" title="January 2014 (8)">Setembre (8)</a>
                                </li>
                                <li>
                                    <a href="blog-page-archive.html" title="February 2014 (7)">Octubre (7)</a>
                                </li>
                                <li>
                                    <a href="blog-page-archive.html" title="March 2014 (6)">Novembre(6)</a>
                                </li>
                                <li>
                                    <a href="blog-page-archive.html" title="April 2014 (5)">Desembre (5)</a>
                                </li>
                                <li>
                                    <a href="blog-page-archive.html" title="May 2014 (4)">Gener (4)</a>
                                </li>
                                <li>
                                    <a href="blog-page-archive.html" title="June 2014 (3)">Febrer (3)</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>

                <!-- Center right column -->
                <li class="template-layout-column-center-right">
                    <h6>
                        <span>Secretaria</span>
                        <span></span>
                    </h6>
                    <div>
                        <p>Nunc gravida rutrum mauris vehin hasellus ac pretium augue vivamul lacus ligul mattis ac vene:</p>
                        <div class="template-component-list template-component-list-style-1-alt template-margin-top-2">
                            <ul>
                                <li>Departament Psicopedagògic i d'orientació</li>
                                <li>Mediació escolar</li>
                                <li>Anglès de de la llar d'infants</li>
                                <li>Tallers d'apmliació i reforç</li>
                                <li>Preparació d'exàmens de Cambridge</li>
                                <li>oferta d'extraescolars</li>
                            </ul>
                        </div>
                    </div>
                </li>

                <!-- Right column -->
                <li class="template-layout-column-right">
                    <h6>
                        <span>Noticies recents</span>
                        <span></span>
                    </h6>
                    <div>
                        <div class="template-widget-recent-post template-widget-recent-post-style-1">
                            <ul>
                                <li>
                                    <a href="post-image.html">
                                        <img src="<?= base_url() ?>img/_sample/150x150/7.jpg" alt="View Post &quot;Drawing Lesson&quot;"/>
                                    </a>
                                    <h6>
                                        <a href="post-image.html" title="View Post &quot;Drawing Lesson&quot;">
                                            Drawing Lesson
                                        </a>
                                    </h6>
                                    <span class="template-icon-blog template-icon-blog-date">October 03, 2014</span>
                                </li>
                                <li>
                                    <a href="post-image.html">
                                        <img src="<?= base_url() ?>img/_sample/150x150/4.jpg" alt="View Post &quot;Fall Parents Meeting Day&quot;"/>
                                    </a>
                                    <h6>
                                        <a href="post-image.html" title="View Post &quot;Fall Parents Meeting Day&quot;">
                                            Fall Parents Meeting Day
                                        </a>
                                    </h6>
                                    <span class="template-icon-blog template-icon-blog-date">October 03, 2014</span>
                                </li>												
                                <li>
                                    <a href="post-image.html">
                                        <img src="<?= base_url() ?>img/_sample/150x150/9.jpg" alt="View Post &quot;Birthday in Kindergarten&quot;"/>
                                    </a>
                                    <h6>
                                        <a href="post-image.html" title="View Post &quot;Birthday in Kindergarten&quot;">
                                            Birthday in Kindergarten
                                        </a>
                                    </h6>
                                    <span class="template-icon-blog template-icon-blog-date">May 20, 2014</span>
                                </li>
                            </ul>
                        </div>									
                    </div>
                </li>

            </ul>

        </div>

    </div>

    <!-- Bottom footer -->
    <div class="template-footer-bottom">
        <div class="template-align-center template-main">
            <div class="template-component-social-icon template-component-social-icon-style-1 template-clear-fix template-margin-bottom-2">
                <ul>
                    <li><a href="#" class="template-component-social-icon-behance"></a></li>
                    <li><a href="#" class="template-component-social-icon-dribbble"></a></li>
                    <li><a href="#" class="template-component-social-icon-envato"></a></li>
                    <li><a href="#" class="template-component-social-icon-facebook"></a></li>
                    <li><a href="#" class="template-component-social-icon-twitter"></a></li>
                </ul>
            </div>
            <div>
                &copy;&nbsp;
                <a href="http://themeforest.net/user/QuanticaLabs/portfolio?ref=QuanticaLabs">Copyright © 2017 - Col·legi Monalco - </a>
                by <a href="http://themeforest.net/user/QuanticaLabs/portfolio?ref=QuanticaLabs">Hipo</a>
                - <a href="http://themeforest.net/user/QuanticaLabs/portfolio?ref=QuanticaLabs">Nota Legal -</a>
            </div>
        </div>
    </div>

</div>
<!-- Go to top button -->
<a href="#go-to-top" class="template-component-go-to-top"></a>