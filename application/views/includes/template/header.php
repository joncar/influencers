
<div class="template-header-top">

    <div class="template-main template-clear-fix">

        <div class="template-header-top-logo">
            <a href="<?= site_url() ?>">
                <img src="<?= base_url() ?>img/logo_header.png" alt="" />
            </a>
        </div>

        <div class="template-header-top-menu template-clear-fix">	

            <nav class="template-component-menu-default">

                <ul class="sf-menu template-clear-fix">

                    <li class="sf-mega-enable-0">
                        <a href="<?= site_url() ?>"><span class="template-icon-menu template-icon-menu-home"></span>Inici</a>                        
                    </li>
                    <li class="sf-mega-enable-1">
                        <a href="<?= site_url('p/qui-som') ?>"><span class="template-icon-menu template-icon-menu-people"></span>L'escola</a>
                        <div class="sf-mega template-layout-25x25x25x25 template-clear-fix">
                            <div class="template-layout-column-left">
                                <span class="sf-mega-header">L'ESCOLA</span>
                                <ul>
                                    <li><a href="<?= site_url('p/historia') ?>">Història</a></li>
                                    <li><a href="<?= site_url('p/quotes') ?>">Quotes</a></li>
                                    <li><a href="<?= site_url('p/instalacions') ?>">Instalacions</a></li>
                                    <li><a href="<?= site_url('p/pec') ?>">PEC</a></li>
                                </ul>
                            </div>
                            <div class="template-layout-column-center-left">
                                <span class="sf-mega-header">PROPOSTA EDUCATIVA</span>
                                <ul>
                                    <li><a href="<?= site_url('p/llar-d-infants') ?>">Llar d'infants</a></li>
                                    <li><a href="<?= site_url('p/infantil') ?>">Infantil</a></li>
                                    <li><a href="<?= site_url('p/primaria') ?>">Primària</a></li>
                                    <li><a href="<?= site_url('p/secundaria') ?>">Secundària</a></li>
                                </ul>												
                            </div>
                            <div class="template-layout-column-center-right">
                                <span class="sf-mega-header">ORGANITZACIÓ</span>
                                <ul>
                                    <li><a href="<?= site_url('p/organigrama') ?>">Organigrama</a></li>
                                    <li><a href="<?= site_url('p/calendari-escolar') ?>">Calendari Escolar</a></li>
                                    <li><a href="<?= site_url('p/horaris') ?>">Horaris</a></li>
                                    <li><a href="<?= site_url('p/consell-escolar') ?>">Consell Escolar</a></li>
                                    <li><a href="<?= site_url('p/ampa') ?>">AMPA</a></li>
                                </ul>												
                            </div>

                            <div class="template-layout-column-right">
                                <span class="sf-mega-header">SECRETARIA</span>
                                <ul>
                                    <li><a href="<?= site_url('documents') ?>">Documents</a></li>
                                    <li><a href="<?= site_url('p/libres-de-text') ?>">Llibres de Text</a></li>
                                    <li><a href="<?= site_url('p/beques-i-subvencions') ?>">Beques i subvencions</a></li>
                                    <li><a href="<?= site_url('autorizacions') ?>">Autoritzacions</a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li class="sf-mega-enable-1">
                        <a href="<?= site_url('p/serveis') ?>"><span class="template-icon-menu template-icon-menu-printer"></span>Activitats i serveis</a>
                        <div class="sf-mega template-layout-50x50 template-clear-fix">
                            <div class="template-layout-column-left">
                                <span class="sf-mega-header">ACTIVITATS</span>
                                <ul>
                                    <?php foreach($this->db->get_where('servicios',array('servicios_categorias_id'=>1))->result() as $l): ?>
                                        <li><a href="<?= site_url('servicios/'. toURL($l->id.'-'.$l->servicios_nombre)) ?>"><?= $l->servicios_nombre ?></a></li>
                                    <?php endforeach ?>
                                </ul>
                            </div>
                            <div class="template-layout-column-right">
                                <span class="sf-mega-header">DOCUMENTS PLA ANUAL</span>
                                <ul>
                                    <?php foreach($this->db->get_where('servicios',array('servicios_categorias_id'=>2))->result() as $l): ?>
                                        <li><a href="<?= site_url('servicios/'. toURL($l->id.'-'.$l->servicios_nombre)) ?>"><?= $l->servicios_nombre ?></a></li>
                                    <?php endforeach ?>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li class="sf-mega-enable-1">
                        <a href="<?= site_url('') ?>"><span class="template-icon-menu template-icon-menu-projector"></span>Projectes</a>
                        <div class="sf-mega template-layout-50x50 template-clear-fix">
                            <div class="template-layout-column-left">
                                <span class="sf-mega-header">PROJECTES CENTRE</span>
                                <ul>
                                    <?php foreach($this->db->get_where('proyectos',array('proyectos_categorias_id'=>2))->result() as $l): ?>
                                        <li><a href="<?= site_url('proyectos/'. toURL($l->id.'-'.$l->proyectos_nombre)) ?>"><?= $l->proyectos_nombre ?></a></li>
                                    <?php endforeach ?>
                                </ul>
                            </div>
                            <div class="template-layout-column-right">
                                <span class="sf-mega-header">PROJECTES TR@AM</span>
                               <ul>
                                   <?php foreach($this->db->get_where('proyectos',array('proyectos_categorias_id'=>2))->result() as $l): ?>
                                        <li><a href="<?= site_url('proyectos/'. toURL($l->id.'-'.$l->proyectos_nombre)) ?>"><?= $l->proyectos_nombre ?></a></li>
                                    <?php endforeach ?>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li class="sf-mega-enable-0">
                        <a href="<?= site_url('') ?>"><span class="template-icon-menu template-icon-menu-speaker"></span>Blog</a>
                        <ul>
                            <?php foreach($this->db->get('blog_categorias')->result() as $l): ?>
                                <li><a href="<?= site_url('blog/categorias/'. toURL($l->id.'-'.$l->blog_categorias_nombre)) ?>"><?= $l->blog_categorias_nombre ?></a></li>
                            <?php endforeach ?>
                        </ul>
                    </li>
                    <li class="sf-mega-enable-0">
                        <a href="<?= site_url('') ?>"><span class="template-icon-menu template-icon-menu-gallery"></span>Galeria</a>
                        <ul>
                            <li><a href="<?= site_url('p/galeria') ?>">Fotografies</a></li>
                            <li><a href="<?= site_url('p/videos') ?>">Vídeos</a></li>
                        </ul>
                    </li>
                    <li class="sf-mega-enable-0">
                        <a href="<?= site_url('p/contact') ?>"><span class="template-icon-menu template-icon-menu-envelope"></span>Contact</a>
                    </li>
                </ul>

            </nav>

            <nav class="template-component-menu-responsive">

                <ul class="template-clear-fix">

                    <li>
                        <a href="#">Inici<span></span></a>
                        <ul>
                            <li>
                                <a href="<?= site_url() ?>">Inici</a>
                            </li>
                            <li>
                                <a href="<?= site_url('p/qui-som') ?>">Qui Som</a>
                            </li>
                            <li>
                                <a href="<?= site_url('p/cicles') ?>">Cicles</a>
                            </li>
                            <li>
                                <a href="<?= site_url('p/serveis') ?>">Serveis</a>
                            </li>
                            <li>
                                <a href="<?= site_url('p/blog') ?>">Blog</a>
                            </li>
                            <li>
                                <a href="<?= site_url('p/contact') ?>">Contact</a>
                            </li>

                        </ul>

                    <li>

                </ul>

            </nav>

        </div>

    </div>
</div>