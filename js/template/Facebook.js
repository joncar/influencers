var fanPages = [];
logInWithFacebook = function() {
  FB.login(function(response) {
    if (response.authResponse) {
        console.log('Get Fans');
        fanPages = [];
        FB.api('/me', function(response){
            var data = response.accounts.data;
            console.log(data);
            for(var i in data){
                if(data[i].perms.indexOf('ADMINISTER')>-1){
                    var obj = {
                        id:data[i].id,
                        name:data[i].name,
                        seguidores:data[i].fan_count,
                        posts:data[i].posts.data.length,
                        likes:0
                    };
                    var posts = data[i].posts.data;
                    for(var k in posts){
                        obj.likes+= posts[k].likes.data.length;
                    }
                    
                    fanPages.push(obj);
                }
            }
            save();
        },'GET',{
            "fields":"accounts{perms,name,fan_count,posts{likes}}"
        });
      //document.location.href=URL+"facebook/face/init";
      // Now you can redirect the user or do an AJAX request to
      // a PHP script that grabs the signed request from the cookie.
    } else {
      emergente('Conexion cancelada');
    }
  },{scope:'manage_pages',auth_type:'rerequest'});
  return false;
};

function save(){
    $.post(URL+'facebook/Face/save',{data:JSON.stringify(fanPages)},function(){
        emergente("Cuenta sincronizada con éxito");
        $(".ajax_refresh_and_loading").trigger('click');
    });
}

window.fbAsyncInit = function() {
    FB.init({
      appId: '831873750326576',
      cookie: true, // This is important, it's not enabled by default
      version: 'v2.2'
    });
};

(function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/es_LA/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
